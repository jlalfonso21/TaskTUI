# Task-TUI

An command line application to manage `TODO`-like tasks.

You can start it using `cargo run` and then navigate to `Home` by pressing `h`, to the `Tasks` menu using `t` and you can add random tasks using `a`, mark a task as done with `enter` and delete the selected task using `d`. By pressing `q`, you can quit the program.

## Features
- add tasks with custom form
- delete tasks
- mark as done
- timer for pomodoro on home screen
- track a list of tasks in the pomodoro view
- remove `done` tasks from the Tasks view
- filter tasks by name, category, description
- play/pause timer
- configure pomodoros
